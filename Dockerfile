FROM ubuntu
# Start with Ubuntu 18.04 as a basis

# Install wget in Ubuntu
FROM ubuntu:18.04
RUN  apt-get update \
  && apt-get install -y wget \
  && rm -rf /var/lib/apt/lists/*
  
# Define a noninteractive session so that installations do not hang on user input
ARG DEBIAN_FRONTEND=noninteractive

# Install sudo and add docker user "xqxp" to sudoers group
RUN apt-get update && \
      apt-get -y install sudo
RUN useradd -m xqxp && echo "xqxp:xqxp" | chpasswd && adduser xqxp sudo
CMD /bin/bash

# Create directories in which to install Perl
RUN  mkdir -p /shared
RUN  mkdir -p /usr/local/lib/perl5/5.16.3

# Set Perl environment variables
ENV PERL_PATH=/shared/perl-5.16.3/
ENV PERL5LIB=/usr/local/lib/perl5/5.16.3 
ENV PATH="$PERL_PATH/bin:$PATH"

# Uninstall default Perl - xQuest requires a specific version
RUN apt-get remove perl -y

# Remove any traces of the newer perl version
RUN rm -rf /usr/bin/perl5.26-x86_64-linux-gnu
RUN rm -rf /usr/lib/x86_64-linux-gnu/perl

# Install Perl 5.16.3 - required for xQuest
WORKDIR /shared
RUN apt-get update -y 
RUN apt-get install -y build-essential
RUN gcc --version
ADD http://www.cpan.org/src/5.0/perl-5.16.3.tar.gz /shared
RUN tar -xzf perl-5.16.3.tar.gz
WORKDIR /shared/perl-5.16.3
RUN ./Configure -de
RUN make
RUN make install
# Install curl
RUN apt-get install -y curl
# Configure cpanm with curl
RUN curl -L http://cpanmin.us | perl - App::cpanminus
# Also install cpanminus to aid CPAN installation
RUN apt-get -y install cpanminus
# Modify the CPAN settings to avoid FTP related network errors by using HTTPS only
ENV PERL_CPANM_OPT --verbose --mirror https://cpan.metacpan.org

#Install git for syncing repositories
RUN apt-get -y update
RUN apt-get -y install git

# Install other required packages
RUN apt-get -y install libexpat1-dev
RUN apt-get -y install pkg-config
RUN apt-get -y install libgd-dev

# Make and move to home/xquest, and clone the xq_xp git repo there
RUN mkdir -p /home/xquest/
WORKDIR /home/xquest
RUN git clone https://gitlab.ethz.ch/leitner_lab/xquest_xprophet.git /home/xquest --depth=1
# Untar files to current location
RUN tar -xvf V2.1.5.tar
# Adjust installation scripts for the non-standard perl path
WORKDIR /home/xquest/V2.1.5/xquest/cgi
RUN sed -i s+/usr/bin/perl+/shared/perl-5.16.3+g changeheader.pl

# Run the xQuest installation script
# In the end, the commands from the installation script are commented out
# In a Docker container, these packages are better installed using the individual commands below
# Otherwise, all the commands in the file have to be edited (the commands below were written to do this)
# WORKDIR /home/xquest/V2.1.5/xquest/installation
# # Replace all "sudo" in install_packages.sh (not required in Docker container) and add "-y" flag
# RUN sed -i "s/apt-get install/apt-get -y install/g" install_packages.sh
# RUN sed -i "s/sudo //g" install_packages.sh
# Replace all "cpan" in install_packages.sh with cpanm, and correct some other typos int he CPAN commands
# RUN sed -i "s/HTML:PageIndex/HTML::PageIndex/g" install_packages.sh
# RUN sed -i "s/Math:Random/Math::Random/g" install_packages.sh
# RUN sed -i "s/cpan/cpanm/g" install_packages.sh
# # Run install packages script
# RUN ./install_packages.sh
# Replace by running line by line commands by runnig separately instead
# The top two are for the apache server (not yet properly configured)
# RUN apt-get -y install apache2
# RUN apt-get -y install libapache-session-perl
# Other packages required for xQuest
RUN apt-get -y install bioperl
RUN apt-get -y install libberkeleydb-perl
RUN apt-get -y install libcgi-fast-perl
RUN apt-get -y install libcgi-session-perl
RUN apt-get -y install libdata-dumper-concise-perl
RUN apt-get -y install libdata-dumper-simple-perl
RUN apt-get -y install libhtml-template-perl
RUN apt-get -y install libfile-copy-recursive-perl
RUN apt-get -y install libgd-graph-perl
RUN apt-get -y install libio-compress-bzip2-perl
RUN apt-get -y install libio-compress-perl
RUN apt-get -y install libtemplate-perl
RUN apt-get -y install libxml-treebuilder-perl
RUN apt-get -y install libxml-writer-perl
RUN apt-get -y install libmldbm-perl
RUN apt-get -y install libstatistics-descriptive-perl
RUN apt-get -y install libcgi-formbuilder-perl
RUN apt-get -y install libmail-sender-perl
RUN apt-get -y install build-essential
RUN apt-get -y install tofrodos
RUN apt-get -y install subversion
# Additional dependencies found during tests and other useful utilities
RUN apt-get -y install libdb-dev
RUN apt-get -y install libssl-dev
RUN apt-get -y install liblist-moreutils-perl
RUN apt-get -y install libhtml-parser-perl
RUN apt-get -y install vim

## Create a softlink for the dos2unix command
WORKDIR /usr/bin
RUN ln -s fromdos dos2unix
WORKDIR /home/xquest/V2.1.5/xquest/installation

# Modify Environment.pm to reflect the container environment
WORKDIR /home/xquest/V2.1.5/xquest/modules
RUN sed -i s+\/home\\\\\/xquest\\\\\/xquest+\/home\\\\\/xquest\\\\\/V2.1.5\\\\\/xquest+g Environment.pm
#RUN sed -i s+\/home\/xquest\/xquest\/conf\/web.config+\/home\/xquest\/V2.1.5\/xquest\/conf\/web.config+g Environment.pm
#RUN sed -i s+\/home\/xquest\/xquest\/deffiles\/mass_table.def+\/home\/xquest\/V2.1.5\/xquest\/deffiles\/mass_table.def+g Environment.pm
#Add xQuest bin to path
ENV PATH="${PATH}:/home/xquest/V2.1.5/xquest/bin"

# Make sure all Perl packaages are available by setting library locations
ENV PERL5LIB="/usr/local/lib/perl5/5.16.3/x86_64-linux/:/usr/local/lib/perl5/5.16.3/:/usr/local/lib/perl5/site_perl/5.16.3/x86_64-linux/:/usr/local/lib/perl5/site_perl/5.16.3/:/usr/local/lib/perl5/5.16.3/x86_64-linux/:/usr/local/lib/perl5/5.16.3/:/usr/bin/:/usr/share/perl5/" 

# Install some further Perl packages with cpanm - these were not found during testing
RUN cpanm --force --notest List::MoreUtils::XS@0.408
RUN cpanm --force --notest HTML::Entities XML::Parser GD DB_File

# Make xQuest executable by other users
WORKDIR /home/xquest/V2.1.5/xquest/bin/
RUN chmod +x ./*

# When done with setup change to xqxp user
# This step is deactivated - creates permission errors
# USER xqxp

# Make example data folder
RUN mkdir -p /home/example_data/
WORKDIR /home/example_data/