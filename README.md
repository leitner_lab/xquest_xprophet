## xQuest/xProphet
Identification and statistical validation of cross-linked peptides identified by MS
Chemical cross-linking of proteins or protein complexes in combination with mass spectrometry is an increasingly used technique to obtain low resolution structural restraints.
The software xQuest is designated to identify cross-linked peptides from LC-MS/MS spectra [1].

### GitLab Wiki
More information can be found on the various pages of the xQuest/xProphet GitLab wiki:
* [Home](https://gitlab.ethz.ch/leitner_lab/xquest_xprophet/-/wikis/home)
* [Download information](https://gitlab.ethz.ch/leitner_lab/xquest_xprophet/-/wikis/Home/Download-and-licence-information)
* [Installation](https://gitlab.ethz.ch/leitner_lab/xquest_xprophet/-/wikis/Home/Installation)
* [How to run](https://gitlab.ethz.ch/leitner_lab/xquest_xprophet/-/wikis/Home/How-to-run)

#### xQuest spectrum
![image](https://gitlab.ethz.ch/leitner_lab/xquest_xprophet/-/wikis/uploads/74e2f22b2b5fcc25540a201763ff1ae9/image.png)

xQuest example spectrum: On the top left corner the sequence of the cross-linked peptide is shown. In the spectrum, matched common fragment ions (green) and cross-linker containing fragment ions (red) are indicated with diamonds. On the peptide sequences, matched ions are indicated with red and green ticks.

The software xProphet is designated to estimate false discovery rates (FDRs) in cross-linking datasets [2]. The software is based on a target-decoy search strategy for cross-linked peptides.

A detailed step by step protocol of the experimental and the computational procedure has been published [3].

#### References
1. Rinner O, Seebacher J, Walzthoeni T, Mueller L, Beck M, Schmidt A, Mueller M, Aebersold R (2008) Identification of cross-linked peptides from large sequence databases. [Nature methods.](http://www.nature.com/nmeth/journal/v5/n4/abs/nmeth.1192.html)
2. Walzthoeni T, Claassen M, Leitner A, Herzog F, Bohn S, Förster F, Beck M, Aebersold R (2012) False discovery rate estimation for cross-linked peptides identified by mass spectrometry. [Nature methods.](http://dx.doi.org/10.1038/nmeth.2103)
3. Leitner A, Walzthoeni T, Aebersold R. (2014) Lysine-specific chemical cross-linking of protein complexes and identification of cross-linking sites using LC-MS/MS and the xQuest/xProphet software pipeline. [Nature protocols.](http://dx.doi.org/10.1038/nprot.2013.168)

